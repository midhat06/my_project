const Transaction = require("../models/transaction")

const { updateBalance } = require("../account/controller")

// const createNewTransaction = tran => {
//     const newTransaction = new transaction(tran)

//     //let amt = transaction.findByIdAndUpdate()
//     return newTransaction.save()
// }
const createNewTransaction = async transaction => {
    const newTransaction = new Transaction(transaction);
    let createdTransaction = await newTransaction.save();
    if (transaction.type == "debit") {
        await updateBalance(transaction.account, transaction.amt * -1);
    } else {
        await updateBalance(transaction.account, transaction.amt);
    }
    return createdTransaction.populate("account").execPopulate();
};

const getTransactionById = id => {
    return Transaction.findById(id).populate("account", {
        accountNumber: 1
    })
}

const getTransaction = () => {
    return Transaction.find().populate("account")
}

const transDeleteById = id => {
    return Transaction.findById(id)
}

module.exports = {
    createNewTransaction: createNewTransaction,
    getTransactionById: getTransactionById,
    getTransaction: getTransaction,
    transDeleteById: transDeleteById
};
