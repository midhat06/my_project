const { Router } = require("express")
const router = Router();
const Account = require("../models/account")

const {
    createNewTransaction,
    getTransactionById,
    getTransaction,
    transDeleteById,

} = require("./controller")


//router.post('/', (req, res) => {

//     //console.log(Customer.customerSchema)//can import only when you export something
//     //console.log("name: "+name+"  mobile: "+mobile)

//     createNewTransaction(req.body)
//         .then(tran => {
//             console.log(tran)
//             let account = req.body.Account
//             //console.log(saccount)
//             let typee = req.body.type
//             console.log(tran.account)
//             console.log(tran.type)
//             Account.findOne({ _id: tran.account })
//                 .then(ac => {
//                     if (tran.type == "debit") {
//                         let num = ac.balance - tran.amt
//                         ac.balance = num
//                         ac.save().then(accc => {
//                             tran.send(accc)
//                         })
//                     }
//                     else {
//                         ac.balance = ac.balance + tran.amt
//                         ac.save().then(accc => {
//                             tran.send(accc)
//                         })

//                     }

//                 })
//                 .catch(err => {
//                     console.log(err)
//                 })
//             // console.log("transaction added")
//             // console.log(tran.account);
//             return res.send(tran)
//         })
//         .catch(err => {
//             console.log(err);
//         })
// });

router.post("/", (req, res) => {
    createNewTransaction(req.body)
        .then(transaction => {
            return res.send(transaction);
        })
        .catch(err => {
            console.log(err);
        });
});


router.get('/', (req, res) => {
    // will return all data {} and if {mobille:76786272} will return only specific
    getTransaction().then(tran => {
        console.log(tran)
        res.send(tran)
    })
        .catch(err => {
            console.log(err)
        })
})


router.get('/:id', (req, res) => {
    getTransactionById(req.params.id) // will return all data {} and if {mobille:76786272} will return only specific
        .then(tran => {
            console.log(tran)
            res.send(tran)
        })
        .catch(err => {
            console.log(err)
            res.sendStatus(500)
        })
})



router.delete('/delete/:id', (req, res) => {
    transDeleteById(req.params.id) // will return all data {} and if {mobille:76786272} will return only specific
        .then(tran => {
            console.log(tran)
            res.destroy(tran)
        })
        .catch(err => {
            console.log(err)
            res.sendStatus(500)
        })
})




module.exports = router;