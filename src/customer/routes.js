const { Router } = require("express")
const router = Router();
//const Customer = require("../models/customer")

const {
    createNewCustomer,
    getCustomerById,
    getCustomers,
} = require("./controller")

router.post('/', (req, res) => {

    //console.log(Customer.customerSchema)//can import only when you export something
    //console.log("name: "+name+"  mobile: "+mobile)

    createNewCustomer(req.body)
        .then(customer => {
            //console.log(customer);
            return res.send(customer)
        })
        .catch(err => {
            console.log(err);
        })
});

router.get('/', (req, res) => {
    // will return all data {} and if {mobille:76786272} will return only specific
    getCustomers().then(cust => {
        console.log(cust)
        res.send(cust)
    })
        .catch(err => {
            console.log(err)
        })
})


router.get('/:id', (req, res) => {
    getCustomerById(req.params.id) // will return all data {} and if {mobille:76786272} will return only specific
        .then(customers => {
            console.log(customers)
            res.send(customers)
        })
        .catch(err => {
            console.log(err)
            res.sendStatus(500)
        })
})

module.exports = router;