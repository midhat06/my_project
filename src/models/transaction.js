//transaction.js

const { model, Schema } = require('mongoose')

const transactionSchema = Schema({
    type: {
        type: String,
        required: true,
        enum: ["debit", "credit"]
    },
    amt: {
        type: String,
        required: true,
    }
    ,
    timestamp: {
        type: Date,
        default: Date.now,
        required: true,
    }
    ,
    account: {
        type: Schema.Types.ObjectId,
        ref: "account",
    }

});

const transactionModel = model('Transaction', transactionSchema);

module.exports = transactionModel;