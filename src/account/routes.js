const { Router } = require("express")
const router = Router();
//const Customer = require("../models/customer")

const {
    createNewAccount,
    getAccountById,
    getAccount,
} = require("./controller")

router.post('/', (req, res) => {

    //console.log(Customer.customerSchema)//can import only when you export something
    //console.log("name: "+name+"  mobile: "+mobile)

    createNewAccount(req.body)
        .then(accc => {
            //console.log(customer);
            return res.send(accc)
        })
        .catch(err => {
            console.log(err);
        })
});

router.get('/', (req, res) => {
    // will return all data {} and if {mobille:76786272} will return only specific
    getAccount().then(acc => {
        console.log(acc)
        res.send(acc)
    })
        .catch(err => {
            console.log(err)
        })
})


router.get('/:id', (req, res) => {
    getAccountById(req.params.id) // will return all data {} and if {mobille:76786272} will return only specific
        .then(ac => {
            console.log(ac)
            res.send(ac)
        })
        .catch(err => {
            console.log(err)
            res.sendStatus(500)
        })
})

module.exports = router;