const Account = require("../models/account")
const createNewAccount = async acc => {
    const newAccount = new Account(acc)
    let createdAccount = await newAccount.save()
    return createdAccount.populate("Customer").execPopulate();
}

const getAccountById = id => {
    return Account.findById(id).populate("Customer")
}

const getAccount = () => {
    return Account.find().populate("Customer")
}

const updateBalance = (id, amount) => {
    return Account.findByIdAndUpdate(id, {
        $inc: {
            balance: amount
        }
    });
};

module.exports = {
    createNewAccount: createNewAccount,
    getAccountById: getAccountById,
    getAccount: getAccount,
    updateBalance: updateBalance
};
