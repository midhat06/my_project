const express = require('express')
const parser = require("body-parser");
const app = express()
app.use(parser.json());

app.use(
    parser.urlencoded({
        extended: false
    })
);



app.get('/', (request, response) => {
    let name = request.query.name

    return response.send(`Hello world ${name}`)


})



app.post('/', (request, response) => {
    let name = request.body.name

    return response.send(`Hi ${name} `)

});

app.listen(3000)